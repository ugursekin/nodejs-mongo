import User from '../../models/User.js'
import jwt from 'jsonwebtoken'
import dotenv from 'dotenv'
// import bcrypt from 'bcryptjs'

dotenv.config()
const { SECRET_KEY } = process.env;

export const Login = async (req, res) => {
    const { email } = req.body
    const user = await User.findOne({ email })

    const nonce = generateNonce()
    const token = jwt.sign({ userID: user._id, nonce }, SECRET_KEY, { expiresIn: '1h' })
    res.status(200).json({ success: true, token: token })
}

function generateNonce() {
    return Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15) + Date.now()
}

// const { name, surname, email } = req.body
// var password = bcrypt.hashSync(req.body.password, 10);
// const newHome = new User({ name, surname, email, password })
// await newHome.save()
// res.status(200).json(newHome)