import User from '../../models/User.js'
import bcrypt from 'bcryptjs'

const LoginMiddleware = async (req, res, next) => {

  const { email, password } = req.body
  const user = await User.findOne({ email })

  if (!user) {
    return res.status(200).json({ success: false, message: 'Girilen bilgiler hatalı lütfen tekrar deneyin.' })
  }

  if (!(bcrypt.compareSync(password, user.password))) {
    return res.status(200).json({ success: false, message: 'Girilen bilgiler hatalı lütfen tekrar deneyin.' })
  }

  next()
}

export default LoginMiddleware