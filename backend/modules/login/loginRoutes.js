import { Router } from 'express'
const router = Router()

import asyncHandler from '../../helpers/asyncHandler.js'
import LoginRequest from './loginRequest.js'
import LoginMiddleware from './loginMiddleware.js'
import { Login } from './loginController.js'

router.post('/', LoginRequest, LoginMiddleware, asyncHandler(Login))

export default router