import Joi from '@hapi/joi'

const LoginRequest = (req, res, next) => {

    const schema = Joi.object({
        email: Joi.string().email().min(9).required(),
        password: Joi.string().min(9).required().regex(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[A-Za-zğüşıöçİĞÜŞÖÇ\d@$!%*?&,.*]+$/),
    })

    const result = schema.validate({ 
        email: req.body.email,
        password: req.body.password,
    })
    
    if (result.error) {
        return res.status(200).json({ success: false, message: 'Lütfen girilen verilerin doğru formatta olduğundan emin olun.' })
    }

    next()
}
  
export default LoginRequest
  