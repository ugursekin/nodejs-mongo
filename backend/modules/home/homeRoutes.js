import { Router } from 'express'
const router = Router()

import asyncHandler from '../../helpers/asyncHandler.js'
import { index } from './homeController.js'

router.get('/', asyncHandler(index))

export default router