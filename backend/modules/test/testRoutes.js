import { Router } from 'express'
const router = Router()

import asyncHandler from '../../helpers/asyncHandler.js'
import { Test } from './testController.js'

router.post('/', asyncHandler(Test))

export default router