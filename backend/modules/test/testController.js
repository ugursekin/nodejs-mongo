
export const Test = async (req, res) => {
    const { token } = req.body
    res.status(200).json({ success: true, token: token })
}