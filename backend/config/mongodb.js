import mongoose from 'mongoose'
import dotenv from 'dotenv'

dotenv.config()

const { DB_USERNAME, DB_PASSWORD, DB_PORT, DB_HOST } = process.env;

async function connectToDatabase() {
  try {
    const dbURL = `mongodb://${DB_USERNAME}:${DB_PASSWORD}@${DB_HOST}:${DB_PORT}/`

    await mongoose.connect(dbURL, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    })

    console.log('MongoDB Bağlantısı Başarılı!')
  } catch (error) {
    console.error('Bağlantı Hatası:', error.message)
  }
}

export default connectToDatabase
