import { Router } from 'express'
const router = Router()

import { AuthTrue, AuthFalse } from './middleware/authMiddleware.js'
import { Refresh } from './middleware/refreshToken.js'

import loginRoutes from './modules/login/loginRoutes.js'
router.use('/login', AuthTrue, loginRoutes)

import testRoutes from './modules/test/testRoutes.js'
router.use('/test', AuthFalse, Refresh, testRoutes)

import homeRoutes from './modules/home/homeRoutes.js'
router.use('/', AuthFalse, Refresh, homeRoutes)

router.use('*', (req, res) => {
    res.send('Sayfa Bulunamadı')
})

export default router