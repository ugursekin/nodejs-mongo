const errorHandler = (err, req, res, next) => {
    console.log(err)
    res.status(500).json({ error: 'Sunucu Hatası: ' + err })
}

export default errorHandler;