
export const AuthFalse = async (req, res, next) => {
    const token = req.body.oldToken
  
    if (!token) {
      return res.status(200).json({ success: false, message: 'Yetkilendirme hatası: Token bulunamadı.' })
    }
  
    next()
  }
  
  export const AuthTrue = async (req, res, next) => {
    const token = req.body.oldToken
  
    if (token) {
      return res.status(200).json({ success: false, message: 'Yetkilendirme hatası: Token bulundu.' })
    }
    
    next()
  }