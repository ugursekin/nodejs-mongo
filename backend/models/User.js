import mongoose from 'mongoose'

const UserSchema = new mongoose.Schema({
  name: { type: String, required: true },
  surname: { type: String, required: true },
  email: { type: String, required: true },
  password: { type: String, required: true },
  department: { type: String, required: true },
  status: { type: Number, required: true, default: 1 },
  authority: { type: Number, required: true, default: 0 },
}, { versionKey: false });

const User = mongoose.model('User', UserSchema);
export default User