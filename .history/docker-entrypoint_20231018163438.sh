#!/bin/bash

# MongoDB'yi başlat
mongod --auth --fork

# Bekleyin, MongoDB başlatılsın
sleep 5

# MongoDB'ye bağlan ve kullanıcıyı oluştur
mongosh --host localhost -u root -p example --authenticationDatabase admin <<EOF
use hommBildirim
db.createUser({user:"homm_bildirim", pwd:"543jlkRE4Aw,", roles:[{role:"dbOwner", db:"hommBildirim"}]})
exit
EOF

# Sonsuz döngüde çalışarak Docker konteynerınızı çalışır halde tut
while true; do
  sleep 1
done
doc