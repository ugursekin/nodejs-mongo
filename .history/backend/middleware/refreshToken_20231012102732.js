 import User from '../models/User.js'
import jwt from 'jsonwebtoken'
import dotenv from 'dotenv'

dotenv.config()
const { SECRET_KEY } = process.env

export const Refresh = async (req, res, next) => {
    const { oldToken } = req.body
    const decodedToken = jwt.verify(oldToken, SECRET_KEY)
    const user = await User.findOne({ _id: decodedToken.userID })

    const nonce = generateNonce()
    req.body.token = jwt.sign({ userID: user._id, nonce }, SECRET_KEY, { expiresIn: '1h' })
    next()
}

function generateNonce() {
    return Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15) + Date.now()
}