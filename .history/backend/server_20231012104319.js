import connectToDatabase from "./config/mongodb.js";
import errorHandler from "./helpers/errorHandler.js";
import router from "./routes.js";
import dotenv from "dotenv";
import express from "express";
import cors from "cors";

const app = express();
dotenv.config();

app.use(express.json());
app.use(express.static("public"));
app.use(cors());
app.use("/", router);
app.use(errorHandler);

const PORT = process.env.PORT;
const HOST = process.env.HOST;

connectToDatabase().then(() => {
  app.listen(PORT, HOST, () => {
    console.log(`http://${HOST}:${PORT} adresinde çalışıyor`);
  });
});
