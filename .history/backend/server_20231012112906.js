// server.js içeriği

import express from 'express'
import { MongoClient } from 'mongodb';
const app = express()

app.get('/', (req, res) => {
    res.send('Selam')
})

// app.js (ya da dilediğiniz bir dosya adı)


// MongoDB bağlantı bilgileri
const uri = 'mongodb://root:example@mongoDB:27017'; // Varsayılan bağlantı URL'si
const dbName = 'myDatabase'; // Veritabanı adı

// MongoDB Client'ı oluşturma ve veritabanına bağlanma
async function createDatabase() {
  try {
    const client = await MongoClient.connect(uri, { useUnifiedTopology: true });
    console.log('MongoDB bağlantısı başarılı.');

    const db = client.db(dbName);

    // Yeni bir koleksiyon oluşturma
    await db.createCollection('myCollection');
    console.log('Koleksiyon oluşturuldu.');

    const result = await collection.insertOne(newDocument);

    if (result.insertedCount === 1) {
      console.log('Yeni belge koleksiyona eklendi.');
    } else {
      console.error('Belge eklenirken bir hata oluştu.');
    }

    client.close();
    console.log('MongoDB bağlantısı kapatıldı.');
  } catch (error) {
    console.error('Hata:', error);
  }
}

// Veritabanını oluşturmak için fonksiyonu çağırın
createDatabase();

const port = 3000
app.listen(port, () => {
    console.log(`Server çalışıyor ${port}`)
})