// server.js içeriği

import express from 'express'
import { MongoClient } from 'mongodb';
const app = express()

app.get('/', (req, res) => {
    res.send('Selam')
})

// app.js (ya da dilediğiniz bir dosya adı)


// MongoDB bağlantı bilgileri
// const uri = 'mongodb://localhost:27017'; // Varsayılan bağlantı URL'si
const uri = "mongodb+srv://ugursekin:123@cluster0.gefaz2j.mongodb.net/?retryWrites=true&w=majority";
const dbName = 'myDatabase'; // Veritabanı adı

// MongoDB Client'ı oluşturma ve veritabanına bağlanma
async function createDatabase() {
  try {
    const client = await MongoClient.connect(uri, { useUnifiedTopology: true });
    console.log('MongoDB bağlantısı başarılı.');

    const db = client.db(dbName);

    // Yeni bir koleksiyon oluşturma
    await db.createCollection('myCollection');
    console.log('Koleksiyon oluşturuldu.');

    client.close();
    console.log('MongoDB bağlantısı kapatıldı.');
  } catch (error) {
    console.error('Hata:', error);
  }
}

// Veritabanını oluşturmak için fonksiyonu çağırın
createDatabase();

const port = 3000
app.listen(port, () => {
    console.log(`Server çalışıyor ${port}`)
})