// server.js içeriği

import express from 'express'
const app = express()

app.get('/', (req, res) => {
    res.send('Bismillah, her hayrın başıdır.')
})

const port = 3030
app.listen(port, () => {
    console.log(`Server çalışıyor ${port}`)
})