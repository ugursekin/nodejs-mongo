// server.js içeriği

import express from 'express'
import { MongoClient } from 'mongodb';
const app = express()

app.get('/', (req, res) => {
    res.send('Selam')
})

// app.js (ya da dilediğiniz bir dosya adı)


// MongoDB bağlantı bilgileri
const uri = 'mongodb://root:example@mongoDB:27017'; // Kullanıcı adı "root", şifre "example", sunucu adı "mongoDB"
const dbName = 'myDatabase'; // Veritabanı adı
const collectionName = 'myCollection'; // Koleksiyon adı

// Yeni bir belge eklemek için veri
const newDocument = {
  field1: 'value55',
  field2: 'value55',
  // Eklemek istediğiniz diğer alanları buraya ekleyin
};

// MongoDB Client'ı oluşturma ve veritabanına bağlanma
async function insertDocument() {
  const client = new MongoClient(uri, { useUnifiedTopology: true });

  try {
    await client.connect(); // MongoDB sunucusuna bağlan
    console.log('MongoDB bağlantısı başarılı.');

    const db = client.db(dbName);
    const collection = db.collection(collectionName);

    const result = await collection.insertOne(newDocument); // Belgeyi ekleyin

    console.log(result);

    if (result.insertedCount === 1) {
      console.log('Yeni belge koleksiyona eklendi2.');
    } else {
      console.error('Belge eklenirken bir hata oluştu2.',error);
    }
  } catch (error) {
    console.error('Hata:', error);
  } finally {
    client.close(); // Bağlantıyı kapatın
    console.log('MongoDB bağlantısı kapatıldı2.');
  }
}

// Veri eklemek için fonksiyonu çağırın
insertDocument();

const port = 3000
app.listen(port, () => {
    console.log(`Server çalışıyor ${port}`)
})